using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private AudioClip enemyDestroyerSound;
        [SerializeField] private float enemyFireSoundVolume = 0.5f;
        [SerializeField] private float enemyDestroyerSoundVolume = 0.5f;


        [SerializeField] private float enemyFireRate = 0.5f;
        private float fireCounter = 0;

        private void Awake()
        {
            Debug.Assert(enemyFireSound != null, "enemyFireSound cannot be null");
            Debug.Assert(enemyDestroyerSound != null, "enemyDestroyerSound cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            
            AudioSource.PlayClipAtPoint(enemyDestroyerSound,Camera.main.transform.position,enemyDestroyerSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= enemyFireRate)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound,Camera.main.transform.position,enemyFireSoundVolume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0f;
            }
        }
    }
}